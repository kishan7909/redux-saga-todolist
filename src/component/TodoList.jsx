import React from "react";
import TodoListItems from "./TodoListItems";

const TodoList = props => {
  return (
    <React.Fragment>
      <TodoListItems id="1" />
      <TodoListItems id="2" />
      <TodoListItems id="3" />
      <TodoListItems id="4" />
    </React.Fragment>
  );
};

export default TodoList;
