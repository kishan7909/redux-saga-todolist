import React from "react";
import { FormGroup, Card } from "reactstrap";

const TodoListItems = props => {
  return (
    <FormGroup>
      <Card className="p-1">
        <div class="custom-control custom-checkbox mr-sm-2">
          <input
            type="checkbox"
            class="custom-control-input m-1"
            id={"customControlAutosizing" + props.id}
          />
          <label
            class="custom-control-label m-1"
            for={"customControlAutosizing" + props.id}
          >
            <spn style={{ textDecoration: "line-through" }}>
              Remember my preference
            </spn>
          </label>
        </div>
      </Card>
    </FormGroup>
  );
};

export default TodoListItems;
