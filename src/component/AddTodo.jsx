import React from "react";
import { FormGroup, Input, Button } from "reactstrap";

const AddTodo = props => {
  return (
    <FormGroup>
      <div className="row">
        <div className="col-md-9">
          <Input type="text" name="todo" placeholder="Enter todo task.." />
        </div>
        <div className="col-md-3">
          <Button color="btn btn-outline-info">Add</Button>
        </div>
      </div>
    </FormGroup>
  );
};

export default AddTodo;
