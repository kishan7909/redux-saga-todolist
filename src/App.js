import React, { Component } from "react";
import { Container } from "reactstrap";
import Todo from "./containers/Todo";

class App extends Component {
  render() {
    return (
      <Container>
        <Todo />
      </Container>
    );
  }
}

export default App;
