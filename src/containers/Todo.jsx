import React, { Component } from "react";
import { Row, Col, Card, CardBody, CardHeader } from "reactstrap";
import AddTodo from "../component/AddTodo";
import TodoList from "../component/TodoList";
export class Todo extends Component {
  render() {
    return (
      <Row>
        <Col md="6" className="offset-md-3">
          <Card>
            <CardHeader className="bg-info text-white">
              <h2>Todo List</h2>
            </CardHeader>
            <CardBody>
              <AddTodo />
              <TodoList />
            </CardBody>
          </Card>
        </Col>
      </Row>
    );
  }
}

export default Todo;
